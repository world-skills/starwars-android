package com.example.starwarsandorid.ui.ui.EditForces

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.starwarsandorid.Datamanager.Datamanager
import com.example.starwarsandorid.R
import com.example.starwarsandorid.model.ForcePlanet
import com.example.starwarsandorid.model.getForcePhoto

class ForcesRecyclerAdapter(val context: Context,var forcePlanets: ArrayList<ForcePlanet> ) : RecyclerView.Adapter<ForcesRecyclerAdapter.ViewHolder>() {

    val inflater = LayoutInflater.from(context)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = inflater.inflate(R.layout.forces_unit_item, parent, false)

        return ViewHolder(viewItem)
    }

    override fun getItemCount(): Int = forcePlanets.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val force = forcePlanets[position]

        holder.forceName.text = force.Name
        holder.forceSize.text = force.size.toString()
        holder.forcePhoto.setImageDrawable(getForcePhoto(force.forceID, context))

        holder.editForce.setOnClickListener {
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.force_inspect_dialog)
            dialog.window?.setLayout(1000, 1000)
            val sizeTxt =  dialog.findViewById<EditText>(R.id.forceSize_editTxt)
            sizeTxt.setText(force.size.toString())

            val saveBtn = dialog.findViewById<Button>(R.id.forceInspectSave_btn)
            saveBtn.setOnClickListener {
                force.setSize(sizeTxt.text.toString().toInt()){
                    forcePlanets = ArrayList(Datamanager.planets.firstOrNull { p -> p.ID == force.planetID }?.Forces!!)
                    notifyItemChanged(position)
                    dialog.hide()
                }
            }


            val deleteBtn = dialog.findViewById<Button>(R.id.forceDelete_btn)
            deleteBtn.setOnClickListener {
                val planet = Datamanager.planets.firstOrNull { it.ID == force.planetID }
                planet?.deleteForce(force){
                    forcePlanets = ArrayList(Datamanager.planets.firstOrNull { p -> p.ID == force.planetID }?.Forces!!)
                    notifyItemRemoved(position)
                    dialog.hide()
                }
            }
            dialog.show()
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val forceName = itemView.findViewById<TextView>(R.id.forceName_txt)
            val forceSize= itemView.findViewById<TextView>(R.id.forceSize_txt)
            val forcePhoto = itemView.findViewById<ImageView>(R.id.forcePhoto_image)
            val editForce = itemView.findViewById<ImageButton>(R.id.forceEdit_btn)
    }
}