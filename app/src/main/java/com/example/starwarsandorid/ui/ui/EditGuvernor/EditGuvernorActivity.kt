package com.example.starwarsandorid.ui.ui.EditGuvernor

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.*
import androidx.core.graphics.drawable.toBitmap
import com.example.starwarsandorid.Datamanager.Datamanager
import com.example.starwarsandorid.R
import com.example.starwarsandorid.extras.PlanetID
import com.example.starwarsandorid.model.Guvernor
import org.w3c.dom.Text

class EditGuvernorActivity : AppCompatActivity() {

    val CAMEAR_MODE = 1
    val GALLERY_MODE = 2


    var guvernor: Guvernor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_guvernor)


        val planetID = intent.getIntExtra(PlanetID, -1)

        guvernor = Datamanager.planets.filter { it.ID == planetID }.firstOrNull()?.Guvernor

        val guvernorPhoto = findViewById<ImageView>(R.id.guvernorPhoto_image)


        if (guvernor?.Photo == null ){
            guvernor?.getPhoto{
                guvernorPhoto.setImageBitmap(it)
            }
        }else{
            guvernorPhoto.setImageBitmap(guvernor?.Photo)
        }

        guvernorPhoto.setOnClickListener {
            val dialog = Dialog(this)
            dialog.setContentView(R.layout.pick_media_dialog)
            dialog.window?.setLayout(1000, 600)
            val actionCamera = dialog.findViewById<TextView>(R.id.cameraPick_txt)
            actionCamera.setOnClickListener {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, CAMEAR_MODE)
                dialog.hide()
            }

            val actionGallery = dialog.findViewById<TextView>(R.id.galleryPick_txt)
            actionGallery.setOnClickListener {
                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, GALLERY_MODE)
                dialog.hide()
            }
            dialog.show()
        }

        val guvernorFirstName = findViewById<EditText>(R.id.guvernorFirstName_editTxt)
        guvernorFirstName.setText(guvernor?.firstName)
        val guvernorLastName = findViewById<EditText>(R.id.guvernorLastName_editTxt)
        guvernorLastName.setText(guvernor?.lastName)

        val guvernorNumberOfPlanets = findViewById<TextView>(R.id.numberOfPlanets_txt)
        guvernorNumberOfPlanets.text = guvernor?.numberOfPlanets.toString()

        val guvernorNumberOfForces = findViewById<TextView>(R.id.numberOfUnits_txt)
        guvernorNumberOfForces.text = guvernor?.numberOfUnits.toString()


        val saveBtn = findViewById<Button>(R.id.guvernorSave_btn)
        saveBtn.setOnClickListener {
            val newFName = guvernorFirstName.text.toString()
            val newLName = guvernorLastName.text.toString()
            guvernor?.firstName = newFName
            guvernor?.lastName = newLName

            guvernor?.saveChanges()

            guvernor?.setNewPhoto(guvernorPhoto.drawable.toBitmap()){
                Toast.makeText(this, "Successfully saved", Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val guvernorPhoto = findViewById<ImageView>(R.id.guvernorPhoto_image)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == CAMEAR_MODE){
                guvernorPhoto.setImageBitmap(data?.extras?.get("data") as Bitmap)
            }

            if (requestCode == GALLERY_MODE){
                guvernorPhoto.setImageURI(data?.data)
            }
        }else{
            Toast.makeText(this, "error opening media", Toast.LENGTH_SHORT).show()
        }

    }
}
