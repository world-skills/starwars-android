package com.example.starwarsandorid.ui.ui.EditForces

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.starwarsandorid.Datamanager.Datamanager
import com.example.starwarsandorid.R
import com.example.starwarsandorid.extras.PlanetID
import com.google.android.material.floatingactionbutton.FloatingActionButton

class EditForcesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_forces)

        val forcesList = findViewById<RecyclerView>(R.id.forcesList_recyclerView)

        val planetID = intent.getIntExtra(PlanetID, -1)

        val planet = Datamanager.planets.firstOrNull { it.ID == planetID }
        forcesList.layoutManager = GridLayoutManager(this, 2)
        forcesList.adapter = ForcesRecyclerAdapter(this, planet?.Forces!!)


        val addForceBtn = findViewById<FloatingActionButton>(R.id.addForce_btn)

        addForceBtn.setOnClickListener{
            val dialog = Dialog(this)

            dialog.setOnDismissListener {
                (forcesList.adapter as ForcesRecyclerAdapter).notifyDataSetChanged()
                Toast.makeText(this, "Force added", Toast.LENGTH_SHORT).show()
            }


            dialog.setContentView(R.layout.forces_dialog)
            dialog.window?.setLayout(window.attributes.width, window.attributes.height)
            val rw = dialog.findViewById<RecyclerView>(R.id.exampleForce_recyclerView)
            Datamanager.getExampleForces{
                rw.adapter = DialogRecyclerAdapter(this, it, planetID, dialog)
            }
            rw.layoutManager = LinearLayoutManager(this)
            dialog.show()
        }
    }
}
