package com.example.starwarsandorid.ui.ui.EditPlanet

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.*
import androidx.core.graphics.drawable.toBitmap
import com.example.starwarsandorid.Datamanager.Datamanager
import com.example.starwarsandorid.R
import com.example.starwarsandorid.extras.PlanetID
import com.example.starwarsandorid.model.Guvernor
import com.example.starwarsandorid.model.Planet
import com.google.android.material.floatingactionbutton.FloatingActionButton

class EditPlanet : AppCompatActivity() {

    var planetID = -1

    val planet: Planet? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_planet)

        val planetTypeSpinner = findViewById<Spinner>(R.id.planetType_spinner)
        val typeAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, Datamanager.PlanetTypes)
        planetTypeSpinner.adapter = typeAdapter

        val planetRegionSpinner = findViewById<Spinner>(R.id.planetRegion_spinner)
        val regionAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, Datamanager.PlanetRegions)
        planetRegionSpinner.adapter = regionAdapter



        val clanSpinner = findViewById<Spinner>(R.id.planetClan_spinner)
        val clanAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, Datamanager.OwnershipClans)
        clanSpinner.adapter = clanAdapter

        planetID = intent.getIntExtra(PlanetID, -1)
        val planet = Datamanager.planets.filter {
            it.ID == planetID
        }.firstOrNull()

        planetRegionSpinner.setSelection(regionAdapter.getPosition(planet?.Region))

        val guvernorSpinner = findViewById<Spinner>(R.id.planetGuvernor_spinner)
        Datamanager.getAllGuvernors {
            val guvernorAdapter =
                ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, it)

            guvernorSpinner.adapter = guvernorAdapter
            val position = it.indexOfFirst { it.firstName == planet?.Guvernor?.firstName && it.lastName == planet.Guvernor.lastName }
            guvernorSpinner.setSelection(position)
        }

        if (planet?.clan == "Galactic republic") {
            guvernorSpinner.visibility = View.INVISIBLE
            findViewById<TextView>(R.id.guvernorTxt_label).visibility = View.INVISIBLE
        }

        planetTypeSpinner.setSelection(typeAdapter.getPosition(planet?.Type))
        clanSpinner.setSelection(clanAdapter.getPosition(planet?.clan))

        val planetImageView = findViewById<ImageView>(R.id.planetPhoto_image)

        if (planet?.Photo == null){
            planet?.getPhoto {
                planetImageView.setImageBitmap(it)
            }
        }else{
            planetImageView.setImageBitmap(planet.Photo)
        }


        val planetEditText = findViewById<EditText>(R.id.planetName_editTxt)
        planetEditText.setText(planet?.Name)

        val changePhotoBtn = findViewById<FloatingActionButton>(R.id.planetChangePhoto_btn)

        changePhotoBtn.setOnClickListener {
            val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickPhoto, 1)
        }


        val saveBtn = findViewById<Button>(R.id.planetSave_btn)

        saveBtn.setOnClickListener {
            val newName = planetEditText.text.toString()
            val newType = planetTypeSpinner.selectedItem.toString()
            val newClan = clanSpinner.selectedItem.toString()
            val newGuvernorID = (guvernorSpinner.selectedItem as Guvernor).ID
            val newRegion = planetRegionSpinner.selectedItem.toString()


            planet?.Name = newName
            planet?.Type = newType
            planet?.clan = newClan
            planet?.guvernorID = newGuvernorID
            planet?.Region = newRegion
            planet?.saveChanges()

            planet?.setNewPhoto(planetImageView.drawable.toBitmap()){
                Toast.makeText(this, "Successfully saved", Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val planetImageView = findViewById<ImageView>(R.id.planetPhoto_image)
        planetImageView.setImageURI(data?.data)
    }
}
