package com.example.starwarsandorid

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.starwarsandorid.extras.PlanetID
import com.example.starwarsandorid.model.Planet
import com.example.starwarsandorid.ui.ui.EditForces.EditForcesActivity
import com.example.starwarsandorid.ui.ui.EditGuvernor.EditGuvernorActivity
import com.example.starwarsandorid.ui.ui.EditPlanet.EditPlanet

class PlanetListRecyclerAdapter(val context: Context,var planets: ArrayList<Planet>) : RecyclerView.Adapter<PlanetListRecyclerAdapter.ViewHolder>() {

    val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlanetListRecyclerAdapter.ViewHolder {
        val itemView = inflater.inflate(R.layout.planet_list_item, parent, false)


        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = planets.size

    override fun onBindViewHolder(holder: PlanetListRecyclerAdapter.ViewHolder, position: Int) {
        val planet = planets[position]

        holder.planetID = planet.ID
        holder.planetRegion.text = planet.Region

        holder.clanName.text = planet.clan



        planet.getPhoto {
            holder.planetPhoto.setImageBitmap(it)
        }
        if (planet.clan == "Galactic republic") {
            holder.guvernorPhoto.visibility = View.INVISIBLE
        }else{
            holder.guvernorPhoto.visibility = View.VISIBLE
        }

        planet.Guvernor.getPhoto {
            holder.guvernorPhoto.setImageBitmap(it)
        }

        holder.planetName.text = planet.Name
        if (context.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
            if (planet.clan == "Galactic republic"){
                holder.firstName.visibility = View.INVISIBLE
                holder.lastName.visibility = View.INVISIBLE
                holder.editGuvernor.visibility = View.INVISIBLE
            }else{
                holder.firstName.visibility = View.VISIBLE
                holder.lastName.visibility = View.VISIBLE
                holder.editGuvernor.visibility = View.VISIBLE
            }


            holder.firstName.text = planet.Guvernor.firstName
            holder.lastName.text = planet.Guvernor.lastName
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var planetID = -1
        val planetName = itemView.findViewById<TextView>(R.id.planetName_txt)
        val firstName = itemView.findViewById<TextView>(R.id.guvernorFirstName_txt)
        val lastName = itemView.findViewById<TextView>(R.id.guvernorLastName_editTxt)
        val editGuvernor = itemView.findViewById<Button>(R.id.editGuvernor_btn)
        val editPlanet = itemView.findViewById<Button>(R.id.editPlanet_btn)
        val editForces = itemView.findViewById<Button>(R.id.editForces_btn)
        val planetPhoto = itemView.findViewById<ImageView>(R.id.planetPhoto_image)
        val guvernorPhoto = itemView.findViewById<ImageView>(R.id.guvernorPhoto_image)
        val planetRegion = itemView.findViewById<TextView>(R.id.planetRegion_txt)
        val clanName = itemView.findViewById<TextView>(R.id.clanName_txt)

        init {
            editPlanet.setOnClickListener {
                val intent = Intent(context, EditPlanet::class.java)
                intent.putExtra(PlanetID, planetID)
                context.startActivity(intent)
            }

            if (context.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                editGuvernor.setOnClickListener {
                    val intent = Intent(context, EditGuvernorActivity::class.java)
                    intent.putExtra(PlanetID, planetID)
                    context.startActivity(intent)
                }

                editForces.setOnClickListener {
                    val intent = Intent(context, EditForcesActivity::class.java)
                    intent.putExtra(PlanetID, planetID)
                    context.startActivity(intent)
                }
            }
        }


    }
}