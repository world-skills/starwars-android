package com.example.starwarsandorid.Datamanager

import android.graphics.Bitmap
import android.util.Base64
import com.example.starwarsandorid.model.Force
import com.example.starwarsandorid.model.Guvernor
import com.example.starwarsandorid.model.Planet
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream

object Datamanager {
    val service = ServiceBuilder.buildService(BackendService::class.java)
    var planets = ArrayList<Planet>()
    var guvernors = ArrayList<Guvernor>()
    val PlanetTypes = arrayOf("Solid", "Star", "Gas Giant", "Water", "Lava")
    val PlanetRegions = arrayOf("The galaxy", "Outer Rim Territories", "Unknown Regions", "Wild space", "Outer Rim", "Mid Rim")

    val OwnershipClans = arrayOf("Confederation", "Galactic republic")

    init {

    }


    fun getExampleForces(onSuccess: (ArrayList<Force>) -> Unit = {}){
        service.getExampleForces().enqueue(object: Callback<ArrayList<Force>>{
            override fun onFailure(call: Call<ArrayList<Force>>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(
                call: Call<ArrayList<Force>>,
                response: Response<ArrayList<Force>>
            ) {
                if (response.isSuccessful){
                    onSuccess(response.body()!!)
                }
            }

        })
    }

    fun getAllGuvernors(onSuccess: (ArrayList<Guvernor>) -> Unit = {}){
        service.getAllGuvernors().enqueue(object: Callback<ArrayList<Guvernor>>{
            override fun onFailure(call: Call<ArrayList<Guvernor>>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(
                call: Call<ArrayList<Guvernor>>,
                response: Response<ArrayList<Guvernor>>
            ) {
                if (response.isSuccessful){
                    guvernors = response.body()!!
                    onSuccess(guvernors)
                }
            }
        })
    }


    fun getAllPlanets(onSuccess: (ArrayList<Planet>) -> Unit = {}){
        service.getAllPlanets().enqueue(object: Callback<ArrayList<Planet>>{
            override fun onFailure(call: Call<ArrayList<Planet>>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(
                call: Call<ArrayList<Planet>>,
                response: Response<ArrayList<Planet>>
            ) {
                if (response.isSuccessful){
                    planets = response.body()!!
                    onSuccess(planets)
                }
            }

        })
    }


    fun searchPlanets(query: String): ArrayList<Planet> {
        val list = planets.filter {
                    it.Name.toLowerCase().contains(query.toLowerCase()) ||
                    it.Region.toLowerCase().contains(query.toLowerCase()) ||
                    it.Type.toLowerCase().contains(query.toLowerCase()) ||
                    it.Guvernor.firstName.toLowerCase().contains(query.toLowerCase()) ||
                    it.Guvernor.lastName.toLowerCase().contains(query.toLowerCase()) ||
                    it.clan.toLowerCase().contains(query.toLowerCase())
        }

        return ArrayList(list)
    }
}

fun BitMapToString(data: Bitmap): String{
    val output = ByteArrayOutputStream()
    data.compress(Bitmap.CompressFormat.PNG, 100, output)

    return Base64.encodeToString(output.toByteArray(), Base64.DEFAULT)
}


