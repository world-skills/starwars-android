package com.example.starwarsandorid.Datamanager

import com.example.starwarsandorid.model.Force
import com.example.starwarsandorid.model.ForcePlanet
import com.example.starwarsandorid.model.Guvernor
import com.example.starwarsandorid.model.Planet
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query

const val apiAddress = "http://192.168.1.8/StarWars_Backend/"

object ServiceBuilder{
  private val client = OkHttpClient.Builder().build()

    private val retrofit = Retrofit.Builder().baseUrl(apiAddress)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    fun<T> buildService(service: Class<T>): T{
        return retrofit.create(service)
    }
}

data class PhotoBody(val PhotoData: String)

interface BackendService{
    @PUT("planet/update-planet")
    fun updatePlanet(@Body planet: Planet): Call<Planet>

    @GET("planet/get-planet-photo")
    fun getPlanetPhoto(@Query("id") id: Int): Call<ResponseBody>

    @PUT("planet/upload-planet-photo")
    fun uploadPlanetPhoto(@Body photoBody: PhotoBody, @Query("id") id: Int): Call<ResponseBody>

    @GET("planet/get-all-planets")
    fun getAllPlanets(): Call<ArrayList<Planet>>

    @PUT("guvernor/update-guvernor")
    fun updateGuvernor(@Body guvernor: Guvernor): Call<ResponseBody>

    @GET("guvernor/get-guvernor-photo")
    fun getGuvernorPhoto(@Query("id") id: Int): Call<ResponseBody>

    @PUT("guvernor/upload-guvernor-photo")
    fun uploadGuvernorPhoto(@Body photoBody: PhotoBody, @Query("id") id: Int): Call<ResponseBody>

    @GET("guvernor/get-all-guvernors")
    fun getAllGuvernors(): Call<ArrayList<Guvernor>>

    @GET("forces/get-example-forces")
    fun getExampleForces(): Call<ArrayList<Force>>

    @PUT("forces/add-force")
    fun addForce(@Body forcePlanet: ForcePlanet): Call<ForcePlanet>

    @PUT("forces/update-force")
    fun updateForce(@Body forcePlanet: ForcePlanet): Call<ResponseBody>

    @PUT("forces/delete-force")
    fun deleteForce(@Query("id") id: Int): Call<ResponseBody>
}