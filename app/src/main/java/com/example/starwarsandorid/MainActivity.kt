package com.example.starwarsandorid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AutoCompleteTextView
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.starwarsandorid.Datamanager.Datamanager
import com.example.starwarsandorid.model.Planet

class MainActivity : AppCompatActivity() {

    var planetListView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        planetListView = findViewById<RecyclerView>(R.id.planetList_recyclerAdapter)

        planetListView?.layoutManager = LinearLayoutManager(this)
        Datamanager.getAllPlanets {
            planetListView?.adapter = PlanetListRecyclerAdapter(this, it)
            val searchView = findViewById<AutoCompleteTextView>(R.id.searchPlanets_editTxt)
            searchView.doOnTextChanged { text, start, count, after ->
                val adapter = planetListView?.adapter as PlanetListRecyclerAdapter
                adapter.planets = Datamanager.searchPlanets(text.toString())
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Datamanager.getAllPlanets {
            planetListView?.adapter = PlanetListRecyclerAdapter(this, it)
        }
    }
}
