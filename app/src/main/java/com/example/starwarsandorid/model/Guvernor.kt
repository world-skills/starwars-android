package com.example.starwarsandorid.model

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.starwarsandorid.Datamanager.BitMapToString
import com.example.starwarsandorid.Datamanager.Datamanager
import com.example.starwarsandorid.Datamanager.PhotoBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Guvernor {
    val ID: Int = -1
    var firstName = "Example First name"
    var lastName = "Example Last name"
    var Photo: Bitmap? = null

    override fun toString(): String = "$firstName $lastName"

    fun getPhoto(onSuccess: (Bitmap) -> Unit = {}){
        Datamanager.service.
        getGuvernorPhoto(ID).
        enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful){
                    val bytes = response.body()?.bytes()
                    Photo = BitmapFactory.decodeByteArray(bytes, 0, bytes?.size!!)
                    onSuccess(Photo!!)
                }
            }
        })
    }

    fun setNewPhoto(photo: Bitmap,onSuccess: () -> Unit= {}){
        Datamanager.service.uploadGuvernorPhoto(PhotoBody(BitMapToString(photo)), ID).enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                onSuccess()
            }

        })
    }

    val numberOfPlanets: Int
    get() = Datamanager.planets.filter { it.guvernorID == ID }.size

    val numberOfUnits: Int
    get(){
        val planets = Datamanager.planets.filter { it.guvernorID == ID }
        var n = 0

        planets.forEach {
            n += it.Forces.size
        }

        return n
    }

    fun saveChanges(onSuccess: () -> Unit = {}){
        Datamanager.service.updateGuvernor(this).enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                onSuccess()
            }

        })
    }


}