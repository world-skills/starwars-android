package com.example.starwarsandorid.model

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.example.starwarsandorid.Datamanager.Datamanager
import com.example.starwarsandorid.R
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForcePlanet{
    val ID = -1
    var Name = "Example Unit"
    var size = 10
    var forceID = -1
    var planetID = -1

    fun setSize(size: Int, onSuccess: () -> Unit = {}){
        this.size = size
        Datamanager.service.updateForce(this).enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful){
                    onSuccess()
                }
            }

        })
    }

}

fun getForcePhoto(forceID: Int,context: Context): Drawable?{
    return when(forceID){
        1 -> ContextCompat.getDrawable(context, R.mipmap.ic_battle_droid)
        2 -> ContextCompat.getDrawable(context, R.mipmap.ic_bx_battle_droid)
        3 -> ContextCompat.getDrawable(context, R.mipmap.ic_driodeka_droid)
        4 -> ContextCompat.getDrawable(context, R.mipmap.ic_dwarf_droid)
        5 -> ContextCompat.getDrawable(context, R.mipmap.ic_general_kalani)
        6 -> ContextCompat.getDrawable(context, R.mipmap.ic_magna_guard)
        7 -> ContextCompat.getDrawable(context, R.mipmap.ic_pollux)
        8 -> ContextCompat.getDrawable(context, R.mipmap.ic_super_battle_droid)
        else -> ContextCompat.getDrawable(context, R.mipmap.ic_tactical_droid)
    }
}

class Force{
    val ID = -1
    val Name = "Example Unit"
}