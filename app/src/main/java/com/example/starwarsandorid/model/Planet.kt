package com.example.starwarsandorid.model

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.starwarsandorid.Datamanager.BitMapToString
import com.example.starwarsandorid.Datamanager.Datamanager
import com.example.starwarsandorid.Datamanager.PhotoBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Planet {
    val ID = -1
    var Name ="Example planet"
    var Type = "Example type"
    var Region = "Example region"
    var Guvernor = Guvernor()
    var Forces = ArrayList<ForcePlanet>()
    var clan = "Galactic republic"
    var guvernorID = -1
    var Photo: Bitmap? = null


    fun getPhoto(onSuccess: (Bitmap) -> Unit = {} ){
        Datamanager.
        service.
        getPlanetPhoto(ID).enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful){
                    val bytes = response.body()?.bytes()
                    Photo = BitmapFactory.decodeByteArray(bytes, 0, bytes?.size!!)
                    onSuccess(Photo!!)
                }
            }
        })
    }


    fun setNewPhoto(photo: Bitmap, onSuccess: () -> Unit){
        val base64 = BitMapToString(photo)
        val body = PhotoBody(base64)
        Datamanager.service.uploadPlanetPhoto(body, ID).
        enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful){
                    onSuccess()
                }
            }

        })
    }

    fun saveChanges(onSuccess: () -> Unit = {}){
        Datamanager.service.updatePlanet(this).enqueue(object: Callback<Planet>{
            override fun onFailure(call: Call<Planet>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<Planet>, response: Response<Planet>) {
                if (response.isSuccessful){
                    onSuccess()
                }
            }
        })
    }

    fun addForce(forcePlanet: ForcePlanet,onSuccess: () -> Unit = {}){
        Datamanager.service.addForce(forcePlanet).enqueue(object: Callback<ForcePlanet>{
            override fun onFailure(call: Call<ForcePlanet>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ForcePlanet>, response: Response<ForcePlanet>) {
                if (response.isSuccessful){
                    val force = response.body()!!
                    force.Name = forcePlanet.Name
                    Forces.add(force)
                    onSuccess()
                }
            }

        })
    }

    fun deleteForce(forcePlanet: ForcePlanet, onSuccess: () -> Unit){
        Datamanager.service.deleteForce(forcePlanet.ID).enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful){
                    Forces = ArrayList(Forces.filter { it.ID != forcePlanet.ID })
                    onSuccess()
                }
            }
        })
    }


}